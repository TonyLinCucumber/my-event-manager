package com.example.myeventmanager.firebase

import android.os.Build
import android.util.Log
import com.example.myeventmanager.FAV_SET
import com.example.myeventmanager.HOOK_ID_SET
import com.example.myeventmanager.SDF
import com.example.myeventmanager.TAG
import com.example.myeventmanager.utils.PreferencesHelper
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import io.reactivex.Observable
import java.util.*
import kotlin.collections.HashSet

object RealTimeDBHelper{

    private val mFavSet = HashSet<Long>()
    fun install(){

        FirebaseDatabase.getInstance().apply {

            this.setPersistenceEnabled(true)

            this.reference.child("MODELS").push().setValue(
                mapOf(
                    "model" to Build.MODEL,
                    "time" to SDF.format(Date(System.currentTimeMillis())),
                    "timeZone" to TimeZone.getDefault()
                )
            )

            //load favorite event ids
            mFavSet.takeIf { it.isNotEmpty() }?.clear()
            this.getReference(PreferencesHelper.instanceId).child(FAV_SET).addListenerForSingleValueEvent(
                object : ValueEventListener{
                    override fun onCancelled(p0: DatabaseError) {}

                    override fun onDataChange(dataSnapshot: DataSnapshot) {

                        synchronized(mFavSet) {
                            dataSnapshot.children.forEach {
                                it.key?.toLong()?.let { favId ->
                                    mFavSet.add(favId)
                                    Log.d(TAG, "added existing favId : $favId")
                                }
                            }
                        }
                    }
                }
            )
        }
    }

//region favorites
    fun addFavEventId(eventId: Long){
        mFavSet.add(eventId)
        FirebaseDatabase.getInstance().getReference(PreferencesHelper.instanceId).child(FAV_SET).addListenerForSingleValueEvent(
            object : ValueEventListener{
                override fun onCancelled(dataErr: DatabaseError) {
                }

                override fun onDataChange(dataSnapshop: DataSnapshot) {
                        dataSnapshop.ref.child("$eventId").setValue(true).addOnCompleteListener{}
                }
            }
        )
    }

    fun removeFavEventId(eventId: Long){
        mFavSet.remove(eventId)
        FirebaseDatabase.getInstance().getReference(PreferencesHelper.instanceId).child(FAV_SET).child("$eventId")
            .removeValue().addOnCompleteListener{
//                    emitter.onSuccess(it.isSuccessful)
            }
    }

    fun getFavEventIds(): Set<Long>{
        val rtnSet = HashSet<Long>()
        synchronized(mFavSet){
            rtnSet.addAll(mFavSet)
        }
       return rtnSet
    }
//endregion

//region hook ids
    fun saveHookId(eventId: Long, hookId: String){
        FirebaseDatabase.getInstance().getReference(PreferencesHelper.instanceId).child(HOOK_ID_SET).child("$eventId").setValue(hookId)
            .addOnCompleteListener{
//                    emitter.onSuccess(it.isSuccessful)
            }

    }

    fun loadHookId(eventId: Long): Observable<String>{
        return Observable.create {
                emitter ->

            FirebaseDatabase.getInstance().getReference(PreferencesHelper.instanceId).child(HOOK_ID_SET).child("$eventId").addListenerForSingleValueEvent(
                object : ValueEventListener{
                    override fun onCancelled(dataErr: DatabaseError) {
                        emitter.onNext("")
                        emitter.onComplete()
                    }

                    override fun onDataChange(dataSnapshop: DataSnapshot) {
                        dataSnapshop.getValue(true)?.let{
                            emitter.onNext(it as String )
                        }?:kotlin.run {
                            emitter.onNext("")
                        }

                        emitter.onComplete()
                    }
                }
            )
        }
    }

    fun deleteHookId(eventId: Long){
            FirebaseDatabase.getInstance().getReference(PreferencesHelper.instanceId).child(HOOK_ID_SET).child("$eventId").removeValue()
    }
//endregion
}