package com.example.myeventmanager.firebase

import android.content.Intent
import android.util.Log
import com.example.myeventmanager.ACTION_PUSH_NOTIFICATION_RECEIVED
import com.example.myeventmanager.utils.PreferencesHelper
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService :  FirebaseMessagingService() {
    private val TAG = "FCM_SERVICE"
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        Log.d(TAG, "From: " + remoteMessage!!.from!!)

        remoteMessage.data.forEach{
            Log.d(TAG, "Message data payload: <${it.key}:${it.value}>")
        }

        remoteMessage.notification?.let {
            Log.d(TAG, "Message Notification Body: ${it.body}, Title: ${it.title}")

            //deal with fcm
            val intent = Intent()
            intent.action = ACTION_PUSH_NOTIFICATION_RECEIVED
            intent.putExtra("title", it.title)
            intent.putExtra("body", it.body)
            this.sendBroadcast(intent)

        }



    }

    override fun onNewToken(newToken: String) {
        super.onNewToken(newToken)

        Log.d(TAG, "Token: $newToken")

        PreferencesHelper.instanceToken = newToken
    }
}
