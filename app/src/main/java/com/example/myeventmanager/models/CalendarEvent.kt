package com.example.myeventmanager.models

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable
import java.text.SimpleDateFormat
import java.util.*

data class CalendarEvent (
    val calendarId: Long,
    var id: Long,
    val title: String,
    val description: String,
    val startTime: Long,
    val endTime: Long,
    val calendarColor: Int,
    val calendarDisplayName: String,
    val reminderMinute: Int,
    val isPushNotificationReminderSet: Boolean
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readLong(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readLong(),
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readInt(),
        when(parcel.readInt()){
            0->false
            else->true
        }
    ) {
    }

    @SuppressLint("SimpleDateFormat")
    override fun toString(): String{

        val sdf = SimpleDateFormat("dd/MM/yy HH:mm:ss")

        val startTimeStr = sdf.format(Date(startTime))
        val endTimeStr = sdf.format(Date(endTime))

        return "calId: $calendarId, id: $id, title: $title, description: $description, start: $startTimeStr, end: $endTimeStr"

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(calendarId)
        parcel.writeLong(id)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeLong(startTime)
        parcel.writeLong(endTime)
        parcel.writeInt(calendarColor)
        parcel.writeString(calendarDisplayName)
        parcel.writeInt(reminderMinute)
        parcel.writeInt(
            when(isPushNotificationReminderSet){
                false-> 0
                true -> 1
            }
        )
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CalendarEvent> {
        override fun createFromParcel(parcel: Parcel): CalendarEvent {
            return CalendarEvent(parcel)
        }

        override fun newArray(size: Int): Array<CalendarEvent?> {
            return arrayOfNulls(size)
        }
    }

}