package com.example.myeventmanager.models

import android.os.Parcel
import android.os.Parcelable

data class CalendarAccount(
    val accountId: Long,
    val accountName: String,
    val accountDisplayName: String,
    val name: String,
    val color: Int
) : Parcelable{

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt()
    ) {
    }

    override fun toString(): String{

        return "id: $accountId, accName: $accountName, accDisplayName: $accountDisplayName, name: $name, color: $color"

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(accountId)
        parcel.writeString(accountName)
        parcel.writeString(accountDisplayName)
        parcel.writeString(name)
        parcel.writeInt(color)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CalendarAccount> {
        override fun createFromParcel(parcel: Parcel): CalendarAccount {
            return CalendarAccount(parcel)
        }

        override fun newArray(size: Int): Array<CalendarAccount?> {
            return arrayOfNulls(size)
        }
    }

}
