package com.example.myeventmanager.screens

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import kotlinx.android.synthetic.main.activity_add_event.*
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import com.example.myeventmanager.models.CalendarAccount
import com.example.myeventmanager.models.CalendarEvent
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import android.content.Intent
import android.content.IntentFilter
import android.graphics.BitmapFactory
import android.os.Build
import com.example.myeventmanager.*
import com.example.myeventmanager.firebase.RealTimeDBHelper
import com.example.myeventmanager.utils.CalendarHelper
import com.example.myeventmanager.utils.PreferencesHelper
import com.example.myeventmanager.utils.schedulePushNotification
import com.google.android.material.snackbar.Snackbar
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class AddEventActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private val TAG = "AddEventActivity"

    val _24HR = 24*60*60*1000
    val _1MIN = 60*1000

    private var mCalendarAccounts : ArrayList<CalendarAccount>? = null

    private var mStartDate: String? = null
    private var mEndDate: String? = null

    private var mStartTime: String? = null
    private var mEndTime: String? = null

    private val DATE_PATERN = "dd/MM/yyyy"
    private val TIME_PATERN = "HH:mm"

    private var mStartDateTime: Date? = null
    private var mEndDateTime: Date? = null

    private lateinit var mCompositeDisposable: CompositeDisposable

    override fun onNothingSelected(parent: AdapterView<*>?) {
        Log.d(TAG, "onNothingSelected")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        Log.d(TAG, "onItemSelected $id ->$position")

        calendarColorBar.backgroundTintList = ColorStateList.valueOf(mCalendarAccounts?.get(position)?.color ?: Color.LTGRAY)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_event)

        mCompositeDisposable = CompositeDisposable()
        initViews()
    }

    override fun onDestroy() {
        mCompositeDisposable.dispose()
        super.onDestroy()
    }

    private fun initViews() {

        intent.extras["calenderAccounts"]?.let {

            mCalendarAccounts = it as ArrayList<CalendarAccount>

            mCalendarAccounts?.let {calendarAccounts->
                val arrays = arrayOfNulls<String>(calendarAccounts.size)

                var myCalOneIndex = -1
                for (i in 0 until calendarAccounts.size) {
                    arrays[i] = calendarAccounts[i].accountDisplayName

                    if (arrays[i] == "My Calendar 1"){
                        myCalOneIndex = i
                    }
                }
                val arrayAdapter = ArrayAdapter(
                    this,
                    android.R.layout.simple_dropdown_item_1line, arrays
                )

                calendarSpinner.adapter = arrayAdapter

                calendarSpinner.onItemSelectedListener = this

                if(myCalOneIndex != -1){
                    calendarSpinner.setSelection(myCalOneIndex)
                }

            }

            setSupportActionBar(findViewById(R.id.bottomAppBar))
            supportActionBar?.let {
                it.setDisplayHomeAsUpEnabled(true)
            }

            //set default stat and end date time
            mStartDateTime = Date(System.currentTimeMillis()+_24HR+(_1MIN*3))

            mStartDate = SimpleDateFormat(DATE_PATERN).format(mStartDateTime)
            newEventStartDateTv.text = "Start Date: $mStartDate"
            mStartTime = SimpleDateFormat(TIME_PATERN).format(mStartDateTime)
            newEventStartTimeTv.text = "Start Time: $mStartTime"

            mEndDateTime = Date(System.currentTimeMillis()+(_24HR*2)+(_1MIN*3))
            mEndDate = SimpleDateFormat(DATE_PATERN).format(mEndDateTime)
            newEventEndDateTv.text = "End Date: $mEndDate"
            mEndTime = SimpleDateFormat(TIME_PATERN).format(mEndDateTime)
            newEventEndTimeTv.text = "End Time: $mEndTime"

            setUpUIClicks()

        }
    }

    private fun setUpUIClicks() {
        insertEventFab.setOnClickListener{

            if (mStartDate == null){

                popInvalidTimeDialog("Please pick the date when the event starts")
                return@setOnClickListener
            }

            if (mEndDate == null){
                popInvalidTimeDialog("Please pick the date when the event ends")
                return@setOnClickListener
            }

            if (mStartTime == null){

                popInvalidTimeDialog("Please pick the time when the event starts")
                return@setOnClickListener
            }

            if (mEndTime == null){
                popInvalidTimeDialog("Please pick the time when the event ends")
                return@setOnClickListener
            }

            mStartDateTime = SimpleDateFormat("$DATE_PATERN $TIME_PATERN").parse("$mStartDate $mStartTime")
            if (mStartDateTime!!.time <= System.currentTimeMillis()){
                popInvalidTimeDialog("The start time of the event has to be in future")
                return@setOnClickListener
            }

            mEndDateTime = SimpleDateFormat("$DATE_PATERN $TIME_PATERN").parse("$mEndDate $mEndTime")

            if (mStartDateTime!!.time >= mEndDateTime!!.time){

                popInvalidTimeDialog("The end time has to be after the start time")
                return@setOnClickListener
            }

            //generate new event
            val selectedCalItemId= calendarSpinner.selectedItemId.toInt()
            Log.d(TAG, "$selectedCalItemId -> ${mCalendarAccounts?.get(selectedCalItemId)?.accountDisplayName}")

            val calendarAccount = mCalendarAccounts!!.get(selectedCalItemId)
            val newEvent = CalendarEvent(
                calendarId = calendarAccount.accountId,
                id = -1,
                title = titleEv.text.toString().trim().takeIf { it.length>0 }?:"No Title Event",
                description = descEv.text.toString().trim(),
                startTime = mStartDateTime!!.time,
                endTime = mEndDateTime!!.time,
                calendarColor = calendarAccount.color,
                calendarDisplayName = calendarAccount.accountDisplayName,
                reminderMinute = when(oneMinLocalAlertSwitch.isChecked){true->2 false->0},
                isPushNotificationReminderSet = pushNotificationSwitch.isChecked
            )

            val isTooLateToSendPushNotification = (newEvent.startTime - System.currentTimeMillis() < _24HR)
            if(newEvent.isPushNotificationReminderSet && !isTooLateToSendPushNotification) {
                var disposable: Disposable? = null
                //schedule push notification
                schedulePushNotification(newEvent.title, newEvent.startTime)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.computation())
                    .doOnSubscribe {
                        disposable = it
                    }
                    .subscribe(
                        {hookId ->
                            Thread.sleep(1000)
                            RealTimeDBHelper.saveHookId(newEvent.id, hookId)
                            Log.d(TAG, "Push notification scheduled id = $hookId")

                            disposable?.takeIf { !it.isDisposed }?.dispose()
                        },
                        { err ->
                            Log.e(TAG, err.localizedMessage)

                            disposable?.takeIf { !it.isDisposed }?.dispose()

                            runOnUiThread{
                                Snackbar.make(findViewById(R.id.mainWrapper), "Failed to schedule Push Notification", Snackbar.LENGTH_SHORT)
                            }
                        }
                    ).let {
                        mCompositeDisposable.add(it)
                    }
            }

            insertEventObservable(newEvent).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .doOnSubscribe{
                    App.INSTANCE?.showProgressDialog(this)
                }.subscribe {newEventId ->
                    App.INSTANCE?.hideProgressDialog()

                    when(newEventId){

                        -1L ->{
                            Snackbar.make(findViewById(R.id.mainWrapper), "Failed to insert event", Snackbar.LENGTH_SHORT).show()
                        }

                        else -> {

                            newEvent.id = newEventId
                            Intent().let {
                                it.putExtra(HAS_NEW_EVENT_BEEN_INSERTED, true)
                                it.putExtra(NEW_EVENT, newEvent)
                                this.setResult(Activity.RESULT_OK, it)
                                this.finish()
                            }
                        }
                    }


                }.let {
                    mCompositeDisposable.add(it)
                }
        }

        startDateFl.setOnClickListener{
            popDatePickerDialog("start")
        }

        startTimeFl.setOnClickListener{
            popTimePickerDialog("start")
        }

        endDateFl.setOnClickListener{
            popDatePickerDialog("end")
        }

        endTimeFl.setOnClickListener{
            popTimePickerDialog("end")
        }

        isPushNotificationSetFl.setOnClickListener{
//            notifyPushNotificationReceived("Title", "body")
            pushNotificationSwitch.performClick()
        }

        isLocalAlertSet.setOnClickListener{
            oneMinLocalAlertSwitch.performClick()
        }
    }

    private fun popDatePickerDialog(startOrEnd: String) {

        val calendar = Calendar.getInstance()
        calendar.time = when(startOrEnd){
            "start" -> {
                 mStartDateTime
            }
            "end"-> {
                mEndDateTime
            }

            else->{ Date()}
        }

        val datePickerDialog = DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val date = SimpleDateFormat(DATE_PATERN).format(calendar.time)
                Log.d(TAG, "$startOrEnd date: $date")

                when(startOrEnd){
                    "start" -> {
                        mStartDate = date
                        newEventStartDateTv.text = "Start Date: $mStartDate"
                    }

                    "end"-> {
                        mEndDate = date
                        newEventEndDateTv.text = "End Date: $mEndDate"
                    }

                }

            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()

    }

    private fun popTimePickerDialog(startOrEnd: String) {

        val calendar = Calendar.getInstance()
        calendar.time = when(startOrEnd){
            "start" -> {
                mStartDateTime
            }
            "end"-> {
                mEndDateTime
            }

            else->{ Date()}
        }

        val timePickerDialog = TimePickerDialog(this,
            TimePickerDialog.OnTimeSetListener { timePicker, i, i1 ->
                calendar.set(Calendar.HOUR_OF_DAY, i)
                calendar.set(Calendar.MINUTE, i1)

                val time = SimpleDateFormat(TIME_PATERN).format(calendar.time)
                Log.d(TAG, "$startOrEnd time: $time")
                when(startOrEnd){
                    "start" -> {
                        mStartTime = time
                        newEventStartTimeTv.text = "Start Time: $mStartTime"

                    }

                    "end"-> {
                        mEndTime = time
                        newEventEndTimeTv.text = "End Time: $mEndTime"

                    }

                }
            }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true
        )
        timePickerDialog.show()

    }

    private fun popInvalidTimeDialog(text: String) {
        AlertDialog.Builder(this)
            .setMessage(text)
            .setPositiveButton(getString(R.string.dialog_ok), null)
            .show()
    }

    private fun insertEventObservable(newEvent: CalendarEvent): Single<Long>{
        return Single.create {
            emitter ->

            var newEventId = CalendarHelper.addCalendarEvent(this, newEvent)
            Log.d(TAG, "$newEvent")
            emitter.onSuccess(newEventId?:-1L)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun notifyPushNotificationReceived(title: String, body: String){

        val notificationBuilder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder(this, NEW_EVENT_NOTIFICATION_CHANNEL_ID)
        } else {
            Notification.Builder(this)
        }.setVisibility(Notification.VISIBILITY_PUBLIC)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
            .setStyle(Notification.BigTextStyle())
            .setContentTitle(title)
            .setContentText(body)


        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(System.currentTimeMillis().toInt(), notificationBuilder.build())

    }

    override fun onStart(){
        super.onStart()
        registerPushNotificationReceiver()
    }

    override fun onStop() {
        unregisterPushNotificationReceiver()
        super.onStop()
    }

    private fun registerPushNotificationReceiver(){
        val intentFilter = IntentFilter(ACTION_PUSH_NOTIFICATION_RECEIVED)

        this.registerReceiver(mPushNotificationReceiver, intentFilter)

    }

    private fun unregisterPushNotificationReceiver(){
        try{
            this.unregisterReceiver(mPushNotificationReceiver)
        }catch (e: Exception){
            Log.e(com.example.myeventmanager.TAG, e.localizedMessage)
        }
    }

    private val mPushNotificationReceiver = object: BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent) {
            when(intent.action){
                ACTION_PUSH_NOTIFICATION_RECEIVED->{
                    val title = intent.getStringExtra("title")
                    val body = intent.getStringExtra("body")
                    notifyPushNotificationReceived(title, body)
                }
            }
        }

    }
}
