package com.example.myeventmanager.screens

import android.app.ActivityOptions
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myeventmanager.*
import com.example.myeventmanager.models.CalendarAccount
import com.example.myeventmanager.models.CalendarEvent
import com.example.myeventmanager.utils.CalendarHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception
import com.example.myeventmanager.firebase.RealTimeDBHelper
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Completable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


class MainActivity : AppCompatActivity(), FavoriteEventFeedBackInterface {

    companion object {
        @JvmStatic
        var removedEventId: Long? = null
    }

    private val mPushNotificationReceiver = PushNotificationReceiver()

    override fun onFavIndicatorClicked(position: Int, eventId: Long) {
        val favSet = RealTimeDBHelper.getFavEventIds()

        if(favSet.contains(eventId)) {
            RealTimeDBHelper.removeFavEventId(eventId)

            //update view
            if(isDisplayFavSw.isChecked){
                RealTimeDBHelper.getFavEventIds().let { favIdSet ->
                    mFilteredData = mData?.filter {
                        val con1 = selectedCalenderAccountIds.contains(it.calendarId)
                        val con2 = favIdSet.contains(it.id)
                       con1 && con2
                    }
                    mAdapter?.setItems(mFilteredData ?: emptyList())
                mAdapter?.notifyItemRangeRemoved(position, 1)
                }
            }else{ mAdapter?.notifyItemChanged(position)}

        }else{
            if (favSet.size<3) {
                RealTimeDBHelper.addFavEventId(eventId)
                mAdapter?.notifyItemChanged(position)
            }else{
                popMaxThreeFavDialog()
            }
        }
    }
//    override fun onFavRemoved() {
//        mAdapter?.notifyItemChanged(0)
//
//       if(isDisplayFavSw.isChecked){
//
//           RealTimeDBHelper.getFavEventIds().let { favIdSet ->
//               mFilteredData = mData?.filter {
//                   val con1 = selectedCalenderAccountIds.contains(it.calendarId)
//                   val con2 = favIdSet.contains(it.id)
//                   con1 && con2
//               }
//
//               mAdapter?.setItems(mFilteredData ?: emptyList())
//               mAdapter?.notifyDataSetChanged()
//           }
//       }
//
//    }
//
//    override fun onFailToAddFav() {
//        popMaxThreeFavDialog()
//    }

    override fun onItemClicked(event: CalendarEvent, shareView: View) {
        val intent = Intent(this, EventDetailsActivity::class.java)
        intent.putExtra("event", event)
        this.startActivity(
            intent,
            ActivityOptions.makeSceneTransitionAnimation(
                this,
                shareView,
                "shareView"
            ).toBundle()
        )
    }

    private var mAdapter: RecyclerViewAdapter? = null
    private var mData: List<CalendarEvent>? = null
    private var mFilteredData: List<CalendarEvent>? = null
    private var mCalendarAccounts : MutableList<CalendarAccount>? = null
    private val selectedCalenderAccountIds = mutableSetOf<Long>()

    private var mInitDataDisposable: Disposable? = null
//    private lateinit var mCompositeDisposable: CompositeDisposable
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        mCompositeDisposable = CompositeDisposable()

        //check the permissions for calender accessing
        if (PermissionActivity.getUngrantedPermissions(this).isNotEmpty()) {
            toPermissionScreen()
            finish()
            return
        } else {
            setContentView(R.layout.activity_main)

            initData().subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe{
                    mInitDataDisposable = it
                    App.INSTANCE?.showProgressDialog(this)
                }
                .subscribe(
                    {
                        initViews()
                        App.INSTANCE?.hideProgressDialog()

                        mInitDataDisposable?.takeIf { !it.isDisposed }?.dispose()
                    },

                    {err ->
                        Log.e(TAG, err.localizedMessage)
                        mInitDataDisposable?.takeIf { !it.isDisposed }?.dispose()

                        App.INSTANCE?.hideProgressDialog()
                        Snackbar.make(findViewById<View>(R.id.wrap_content), "Failed to load data", Snackbar.LENGTH_LONG).show()
                    }
                )
        }
    }

    override fun onDestroy() {
        mInitDataDisposable?.takeIf { !it.isDisposed }?.dispose()
        super.onDestroy()
    }
    override fun onStart(){
        super.onStart()

        removedEventId?.let { eventId ->

            removedEventId = null

            mData = mData?.filter { it.id != eventId }
            mFilteredData = mFilteredData?.filter { it.id != eventId }

            mAdapter?.setItems(mFilteredData?: emptyList())
            mAdapter?.notifyDataSetChanged()
        }

        registerPushNotificationReceiver()

//        Log.d(TAG, "1 ${PreferencesHelper.getFavEventIds().size}")
    }

    override fun onStop() {

//        Log.d(TAG, "2 ${PreferencesHelper.getFavEventIds().size}")

        unregisterPushNotificationReceiver()
        super.onStop()
    }

    private fun toPermissionScreen() {
        Intent(this, PermissionActivity::class.java).let {
            startActivity(it)
        }
    }

    private fun initData(isReloadCalendarAccounts: Boolean = true): Completable {

        mInitDataDisposable?.takeIf { !it.isDisposed }?.dispose()

        return Completable.create {
            emitter ->

            if (isReloadCalendarAccounts) {
                mCalendarAccounts = CalendarHelper.queryCalendarAccounts(this).let {
                    it.forEach { selectedCalenderAccountIds.add(it.accountId) }
                    it
                }
            }

            mData = CalendarHelper.queryCalendarEvents(this)

            //tidy Up fav set
            val allEventIds = HashSet<Long>().let {
                it.addAll(mData!!.map { it.id})
                it
            }
            mData?.map { it.id }?.toSet()?.let {
                RealTimeDBHelper.getFavEventIds().let {
                    it.forEach{favEventId->
                        Log.d(TAG, "contains ${allEventIds.contains(favEventId)}")
                        if(!allEventIds.contains(favEventId)){
                            RealTimeDBHelper.removeFavEventId(favEventId)
                        }
                    }
                }
            }

            emitter.onComplete()
        }

    }

    private fun initViews() {

        setSupportActionBar(bottomAppBar)

        supportActionBar?.setDisplayUseLogoEnabled(true)

        val screenWidth = getScreenWidthDp()
        when {
            screenWidth >= 1200 -> {
                val gridLayoutManager = GridLayoutManager(this, 3)
                eventRecyclerView.layoutManager = gridLayoutManager
            }
            screenWidth >= 800 -> {
                val gridLayoutManager = GridLayoutManager(this, 2)
                eventRecyclerView.layoutManager = gridLayoutManager
            }
            else -> {
                val linearLayoutManager = LinearLayoutManager(this)
                eventRecyclerView.layoutManager = linearLayoutManager
            }
        }

        mAdapter = RecyclerViewAdapter(this)
        mAdapter?.feedBackInterface = this
        eventRecyclerView.adapter = mAdapter
        mData?.let {data->
            mAdapter?.setItems(data)
            mAdapter?.notifyDataSetChanged()
        }

        isDisplayFavSw.setOnCheckedChangeListener { _, isChecked ->
            RealTimeDBHelper.getFavEventIds().let {favIdSet->
                mFilteredData = mData?.filter {
                    val con1 = selectedCalenderAccountIds.contains(it.calendarId)

                    val con2 = if (isChecked) {
                        favIdSet.contains(it.id)
                    } else {
                        true
                    }

                    con1 && con2
                }
                mAdapter?.setItems(mFilteredData?: emptyList())
                mAdapter?.notifyDataSetChanged()
            }
        }

        setupBottomAppBar()
        setupUiClicks()
    }

    private fun setupUiClicks() {

        addEventFab.setOnClickListener {
            toAddEventScreen()
        }



    }

    private fun toAddEventScreen(){
        Intent(this, AddEventActivity::class.java).let {

            val arrayList = ArrayList<CalendarAccount>()
            this.mCalendarAccounts?.forEach {
                arrayList.add(it)
            }
            it.putParcelableArrayListExtra("calenderAccounts", arrayList)
            startActivityForResult(it, INSERT_EVENT_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            INSERT_EVENT_REQUEST_CODE ->{
                data?.extras?.getBoolean(HAS_NEW_EVENT_BEEN_INSERTED)?.let{ wasInserted->
                    if(wasInserted){

                        val insertedEvent = data?.extras?.getParcelable(NEW_EVENT) as? CalendarEvent
                        if (insertedEvent!=null){
                            val mutableList = mutableListOf<CalendarEvent>()
                            mutableList.addAll(mData?: emptyList())
                            mutableList.add(insertedEvent)

                            mData = mutableList.sortedWith(Comparator { o1, o2 ->
                                when(o1.startTime >= o2.startTime){
                                    true -> 1
                                    false -> -1
                                }
                            })

                            refreshData()

                        }else {
                            initData().subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe{
                                    mInitDataDisposable = it
                                    App.INSTANCE?.showProgressDialog(this)
                                }
                                .subscribe(
                                    {
                                        refreshData()
                                        App.INSTANCE?.hideProgressDialog()

                                        mInitDataDisposable?.takeIf { !it.isDisposed }?.dispose()
                                    },

                                    {err ->
                                        Log.e(TAG, err.localizedMessage)
                                        mInitDataDisposable?.takeIf { !it.isDisposed }?.dispose()

                                        App.INSTANCE?.hideProgressDialog()
                                        Snackbar.make(findViewById<View>(R.id.wrap_content), "Failed to load data", Snackbar.LENGTH_LONG).show()
                                    }
                                )
                        }

                    }

                }
            }
        }
    }

    private fun refreshData() {
        RealTimeDBHelper.getFavEventIds().let{ favIdSet->
                mFilteredData = mData?.filter {
                    val con1 = selectedCalenderAccountIds.contains(it.calendarId)

                    var con2 = true
                    if (isDisplayFavSw.isChecked){
                        con2 = favIdSet.contains(it.id)
                    }

                    con1 && con2
                }

                mAdapter?.setItems(mFilteredData?: emptyList())
            }
    }

    private fun setupBottomAppBar() {

        bottomAppBar.fabCradleMargin =  17f //initial default value 17f
        bottomAppBar.fabCradleRoundedCornerRadius =  28f //initial default value 28f

        bottomAppBar.invalidate()
    }

    private fun getScreenWidthDp(): Int {
        val displayMetrics = resources.displayMetrics
        return (displayMetrics.widthPixels / displayMetrics.density).toInt()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.popup_menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when(item.itemId){
            R.id.filter_calendars ->{
                popFilterCalendersDialog()
                true
            }

            R.id.build_info ->{
                popBuildInfoDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    private fun popFilterCalendersDialog(){

        val displayNameList = mCalendarAccounts?.map { Pair(it.accountId, it.accountDisplayName)} ?: emptyList()

        val displayNames = arrayOfNulls<String>(displayNameList.size)
        val checkedItems = BooleanArray(displayNameList.size)
        for(i in 0 until displayNames.size){
            displayNames[i] = displayNameList[i].second

            if(selectedCalenderAccountIds.contains(displayNameList[i].first)) {
                checkedItems[i] = true
            }
        }


        AlertDialog.Builder(this)
            .setTitle(getString(R.string.choose_calendars))
            .setMultiChoiceItems(displayNames, checkedItems) { _, which, isChecked ->
                checkedItems[which] = isChecked
            }

            .setPositiveButton(getString(R.string.dialog_ok)) { dialog, _->
                for(i in 0 until checkedItems.size){
                    if (checkedItems[i]){
                        selectedCalenderAccountIds.add(displayNameList[i].first)
                    }else{
                        selectedCalenderAccountIds.remove(displayNameList[i].first)
                    }
                }

                RealTimeDBHelper.getFavEventIds().let{favIdSet->
                        mFilteredData = mData?.filter {
                            val con1 = selectedCalenderAccountIds.contains(it.calendarId)

                            val con2 = if(isDisplayFavSw.isChecked){
                                favIdSet.contains(it.id)
                            }else{
                                true
                            }

                            con1&&con2
                        } ?: emptyList()
                        mAdapter?.setItems(mFilteredData?: emptyList())
                        dialog.dismiss()
                    }
            }
            .setNegativeButton(getString(R.string.dialog_cancel), null)
            .show()
    }

    private fun popBuildInfoDialog(){
        AlertDialog.Builder(this)
            .setTitle("Ver. ${BuildConfig.VERSION_NAME}")
            .setMessage("This app is for Tony Lin's Android development skills assessment only")
            .setPositiveButton(getString(R.string.dialog_ok), null)
            .show()
    }

    private fun popMaxThreeFavDialog(){
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.max_three_fav))
            .setPositiveButton(getString(R.string.dialog_ok), null)
            .show()
    }

    private fun popPushNotificationDialog(title: String, body: String){
        AlertDialog.Builder(this)
            .setTitle("Push Notification Received")
            .setMessage("$body : $title")
            .setPositiveButton(getString(R.string.dialog_ok), null)
            .show()
    }

    private fun registerPushNotificationReceiver(){
        val intentFilter = IntentFilter(ACTION_PUSH_NOTIFICATION_RECEIVED)

        this.registerReceiver(mPushNotificationReceiver, intentFilter)

    }

    private fun unregisterPushNotificationReceiver(){
        try{
            this.unregisterReceiver(mPushNotificationReceiver)
        }catch (e: Exception){
            Log.e(TAG, e.localizedMessage)
        }
    }

    inner class PushNotificationReceiver: BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent) {
            when(intent.action){
                ACTION_PUSH_NOTIFICATION_RECEIVED->{
                    val title = intent.getStringExtra("title")
                    val body = intent.getStringExtra("body")
                    popPushNotificationDialog(title, body)
                }
            }
        }

    }

//    private fun tidyUpFavSet(){
//        Thread{
//            val favSet = PreferencesHelper.getFavEventIds()
//            mData?.forEach{
//
//
//            }
//
//        }.start()
//    }
}