package com.example.myeventmanager.screens

import android.animation.ObjectAnimator
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.myeventmanager.App
import com.example.myeventmanager.R
import com.example.myeventmanager.SDF
import com.example.myeventmanager.firebase.RealTimeDBHelper
import com.example.myeventmanager.models.CalendarEvent
import com.example.myeventmanager.utils.CalendarHelper
import com.example.myeventmanager.utils.HOOK_API_KEY
import com.example.myeventmanager.utils.HookAPI
import com.example.myeventmanager.utils.PreferencesHelper
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_event_details.*
import java.util.*

class EventDetailsActivity : AppCompatActivity() {

    private val TAG = "EventDetailsActivity"
    private lateinit  var mEvent: CalendarEvent
    private lateinit var mCompositeDisposable: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_details)

        mCompositeDisposable = CompositeDisposable()

        mEvent = intent.getParcelableExtra("event")

        initView()
    }

    override fun onDestroy() {
        mCompositeDisposable.dispose()
        super.onDestroy()
    }

    private fun initView() {
        calendarDisplayNameTv.text = mEvent.calendarDisplayName
        calendarDisplayNameTv.backgroundTintList = ColorStateList.valueOf(mEvent.calendarColor)

        cardShareView.setOnTouchListener(mTouchListener)
        calendarColor.backgroundTintList = ColorStateList.valueOf(mEvent.calendarColor)
        favIv.imageTintList = ColorStateList.valueOf(mEvent.calendarColor)
        localReminderTv.setTextColor(ColorStateList.valueOf(mEvent.calendarColor))
        pnIv.imageTintList = ColorStateList.valueOf(mEvent.calendarColor)
        titleTv.setTextColor(ColorStateList.valueOf(mEvent.calendarColor))
        descTv.setTextColor(ColorStateList.valueOf(mEvent.calendarColor))
        startTimeTv.setTextColor(ColorStateList.valueOf(mEvent.calendarColor))
        endTimeTv.setTextColor(ColorStateList.valueOf(mEvent.calendarColor))

        RealTimeDBHelper.getFavEventIds().let{ favEventIds->
            favIv.setImageResource(
                when (favEventIds.contains(mEvent.id)){
                    true -> R.drawable.ic_check_black_24dp
                    false-> R.drawable.ic_close_black_24dp
                }
            )
        }

        titleTv.text = mEvent.title
        descTv.text = mEvent.description
        startTimeTv.text = SDF.format(Date(mEvent.startTime))
        endTimeTv.text = SDF.format(Date(mEvent.endTime))

        localReminderTv.text =  when(mEvent.reminderMinute) {
            0 -> "Not Set"
            else -> resources.getQuantityString(R.plurals.minutes, mEvent.reminderMinute, mEvent.reminderMinute)
        }

        pnIv.setImageResource(
            when (mEvent.isPushNotificationReminderSet){
                true -> R.drawable.ic_check_black_24dp
                false-> R.drawable.ic_close_black_24dp
            }
        )

        setSupportActionBar(bottomAppBar)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
        }

        deleteEventFab.setOnClickListener(View.OnClickListener {
            popDeleteConfirmDialog()
        })
    }

    private fun popDeleteConfirmDialog() {

        AlertDialog.Builder(this)
            .setTitle(getString(R.string.confirm_delete_event_title))
            .setMessage(getString(R.string.confirm_delete_event_msg))
            .setPositiveButton(getString(R.string.dialog_yes)){
                _, _ ->
                deleteEventObservable()
                    .observeOn(Schedulers.computation())
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {
                        App.INSTANCE?.showProgressDialog(this)
                    }
                    .subscribe{ isSuccess ->
                        App.INSTANCE?.hideProgressDialog()
                        when(isSuccess){
                            true -> {
                                RealTimeDBHelper.removeFavEventId(mEvent.id)

                                RealTimeDBHelper.loadHookId(mEvent.id).subscribeOn(Schedulers.io())
                                    .observeOn(Schedulers.io())
                                    .subscribe {deletedEventId->
                                        var disposable: Disposable? = null
                                        disposable = HookAPI.deleteHook(HOOK_API_KEY, deletedEventId).subscribeOn(Schedulers.io())
                                            .observeOn(Schedulers.computation())
                                            .subscribe(
                                                {
                                                    RealTimeDBHelper.deleteHookId(mEvent.id)
                                                    disposable?.takeIf { !it.isDisposed }?.dispose()
                                                },
                                                {err ->
                                                    Log.e(TAG, err.localizedMessage)
                                                    RealTimeDBHelper.deleteHookId(mEvent.id)
                                                    disposable?.takeIf { !it.isDisposed }?.dispose()
                                                }
                                            )
                                    }.let { mCompositeDisposable.add(it) }

                                MainActivity.removedEventId = mEvent.id
                                finish()
                            }

                            false->{
                                Snackbar.make(mainWrapper,
                                    R.string.failed_to_delete_event_txt, Snackbar.LENGTH_SHORT).show()
                            }
                        }
                }.let { mCompositeDisposable.add(it) }
            }
            .setNegativeButton(getString(R.string.dialog_no), null)
            .show()
    }


    private fun deleteEventObservable(): Single<Boolean> {
        return Single.create {
            emitter ->
            emitter.onSuccess(CalendarHelper.deleteCalendarEvent(this,mEvent.id))
        }
    }

    private var mTouchListener: View.OnTouchListener = View.OnTouchListener { view, motionEvent ->
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                val upAnim = ObjectAnimator.ofFloat(view, "translationZ", 0F)
                upAnim.setDuration(200)
                upAnim.setInterpolator(DecelerateInterpolator())
                upAnim.start()
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                val downAnim = ObjectAnimator.ofFloat(view, "translationZ", 20F)
                downAnim.setDuration(200)
                downAnim.setInterpolator(AccelerateInterpolator())
                downAnim.start()
            }
        }
        false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
