package com.example.myeventmanager.screens

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.myeventmanager.CALENDAR_ACCESS_PERMS
import com.example.myeventmanager.CALENDAR_PERMISSION_REQUEST_CODE
import com.example.myeventmanager.R
import kotlinx.android.synthetic.main.activity_permission.*

class PermissionActivity : Activity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_permission)
        grantBtn.setOnClickListener{requestPermissionsIfNeeded()}

        YoYo.with(Techniques.FadeIn).duration(1000).playOn(imageView)
    }

    private fun toMainScreen() {
        Intent(this, MainActivity::class.java).let {
            startActivity(it)
        }
    }


    companion object {
        @JvmStatic
        fun getUngrantedPermissions(context: Context): ArrayList<String>{
            val permList = ArrayList<String>()
            if (ContextCompat.checkSelfPermission(context, CALENDAR_ACCESS_PERMS[0]) != PackageManager.PERMISSION_GRANTED){
                permList.add(CALENDAR_ACCESS_PERMS[0])
            }

            if (ContextCompat.checkSelfPermission(context, CALENDAR_ACCESS_PERMS[1]) != PackageManager.PERMISSION_GRANTED){
                permList.add(CALENDAR_ACCESS_PERMS[1])
            }

            return permList
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when(requestCode){

            CALENDAR_PERMISSION_REQUEST_CODE -> {

                if (getUngrantedPermissions(this).isEmpty()){
                    toMainScreen()
                }else{
//                    showPermissionSnackbar()
                }
            }
        }
    }

    private fun requestPermissionsIfNeeded(): Boolean{
        val ungrantedPermissions =
            getUngrantedPermissions(this)


        if (ungrantedPermissions.isEmpty()){
            toMainScreen()

            finish()

            return false
        }else{

            val array = arrayOfNulls<String>(ungrantedPermissions.size)
            ungrantedPermissions.toArray(array)

            ActivityCompat.requestPermissions(this, array, CALENDAR_PERMISSION_REQUEST_CODE)

            return true
        }
    }


}
