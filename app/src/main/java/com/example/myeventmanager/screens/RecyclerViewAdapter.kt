package com.example.myeventmanager.screens

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.myeventmanager.R
import com.example.myeventmanager.SDF
import com.example.myeventmanager.firebase.RealTimeDBHelper
import com.example.myeventmanager.models.CalendarEvent
import java.util.*

class RecyclerViewAdapter(private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    private val mItems: MutableList<CalendarEvent>
    private var parentView: View? = null

    private val FAV_COLOR = ContextCompat.getColor(context, R.color.colorAccent)

    var feedBackInterface: FavoriteEventFeedBackInterface? = null

    init {
        mItems = ArrayList()
    }

    fun setItems(data: List<CalendarEvent>) {
        this.mItems.clear()
        this.mItems.addAll(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        parentView = parent
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_view, parent, false)
        return RecyclerViewHolder(view)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is RecyclerViewHolder) {

            val event = mItems[position]

            val animation = AnimationUtils.loadAnimation(context,
                R.anim.anim_recycler_item_show
            )
            holder.mView.startAnimation(animation)

            val aa1 = AlphaAnimation(1.0f, 0.1f)
            aa1.duration = 400
            holder.sharedRound.startAnimation(aa1)

            val aa = AlphaAnimation(0.1f, 1.0f)
            aa.duration = 400

            holder.sharedRound.startAnimation(aa)

            holder.mView.setOnClickListener {
                feedBackInterface?.onItemClicked(event, holder.sharedRound)
            }

            holder.sharedRound.backgroundTintList = ColorStateList.valueOf(event.calendarColor)
            holder.titleTv.text = event.title
            holder.startTimeTv.text = "Start Time: ${SDF.format(Date(event.startTime))}"
            holder.endTimeTv.text = "End Time: ${SDF.format(Date(event.endTime))}"


           RealTimeDBHelper.getFavEventIds().let { favIdSet ->
               holder.favIndicator.imageTintList = when (favIdSet.contains(event.id)) {
                   false -> ColorStateList.valueOf(Color.LTGRAY)
                   true -> ColorStateList.valueOf(FAV_COLOR)
               }
           }


            holder.favIndicator.setOnClickListener{favIndicator->
                YoYo.with(Techniques.Pulse).duration(250).playOn(favIndicator)
                feedBackInterface?.onFavIndicatorClicked(position, event.id)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    private inner class RecyclerViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {

        val sharedRound: RelativeLayout
        val titleTv: TextView
        val startTimeTv: TextView
        val endTimeTv: TextView
        val favIndicator: ImageView

        init {
            sharedRound = mView.findViewById(R.id.rela_round)
            titleTv = mView.findViewById(R.id.title_tv)
            startTimeTv = mView.findViewById(R.id.start_time_tv)
            endTimeTv = mView.findViewById(R.id.end_time_tv)
            favIndicator = mView.findViewById(R.id.fav_iv)
        }
    }
}

interface FavoriteEventFeedBackInterface{
    fun onFavIndicatorClicked(position: Int, eventId: Long)

    fun onItemClicked(event: CalendarEvent, shareView: View)
}