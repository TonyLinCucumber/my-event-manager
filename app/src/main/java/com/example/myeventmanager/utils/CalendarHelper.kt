package com.example.myeventmanager.utils

import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.provider.CalendarContract
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.core.database.getIntOrNull
import androidx.core.database.getLongOrNull
import androidx.core.database.getStringOrNull
import com.example.myeventmanager.MINUTES_OF_24_HOURS
import com.example.myeventmanager.R
import com.example.myeventmanager.models.CalendarAccount
import com.example.myeventmanager.models.CalendarEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

private const val TAG = "CalendarHelper"

private const val CALANDER_URL = "content://com.android.calendar/calendars";
private const val CALANDER_EVENT_URL = "content://com.android.calendar/events";
private const val CALANDER_REMIDER_URL = "content://com.android.calendar/reminders";

private const val CALENDARS_NAME_1 = "My_Testing_Calendar_1"
private const val CALENDARS_NAME_2 = "My_Testing_Calendar_2"
private const val CALENDARS_DISPLAY_NAME_1 = "My Calendar 1"
private const val CALENDARS_DISPLAY_NAME_2 = "My Calendar 2"
private const val CALENDARS_ACCOUNT_NAME = "tonylin928627@gmail.com"
private const val CALENDARS_ACCOUNT_TYPE = "com.android.exchange"

object CalendarHelper{
    //region calendar accounts
    fun queryCalendarAccounts(context: Context): MutableList<CalendarAccount> {
        val userCursor = context.contentResolver.query(Uri.parse(CALANDER_URL), null, null, null, null);
        userCursor.use { _userCursor ->
            if (_userCursor == null) {
                return mutableListOf<CalendarAccount>()
            }

            val calenderNameSet = mutableSetOf<String>()
            Log.d(TAG, "calendar count: ${_userCursor.count}")
            val accountList = mutableListOf<CalendarAccount>()
            while(_userCursor.moveToNext()){

                val accountId = _userCursor.getLong(_userCursor.getColumnIndex(CalendarContract.Calendars._ID))
                val accountName = _userCursor.getStringOrNull(_userCursor.getColumnIndex(CalendarContract.Calendars.ACCOUNT_NAME))
                val accountDisplayName = _userCursor.getStringOrNull(_userCursor.getColumnIndex(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME))
                val name = _userCursor.getStringOrNull(_userCursor.getColumnIndex(CalendarContract.Calendars.NAME))
                val color = _userCursor.getIntOrNull(_userCursor.getColumnIndex(CalendarContract.Calendars.CALENDAR_COLOR))

                Log.d(TAG, "id: $accountId, name: $accountName")
                accountList.add(CalendarAccount(
                    accountId = accountId,
                    accountName = accountName ?: "No Account Name",
                    accountDisplayName = accountDisplayName ?: "No Display Name",
                    name = name ?: "No Name",
                    color = color ?: Color.GRAY
                ))

                name?.let {
                    calenderNameSet.add(it)
                }

            }

            if (!calenderNameSet.contains(CALENDARS_NAME_1)){
                addCalendarAccount(context, CALENDARS_NAME_1)?.let {
                    accountList.add(it)
                }
            }
            if (!calenderNameSet.contains(CALENDARS_NAME_2)){
                addCalendarAccount(context, CALENDARS_NAME_2)?.let {
                    accountList.add(it)
                }
            }
            return accountList
        }
    }

    private fun addCalendarAccount(context: Context, calendarName: String): CalendarAccount? {

        val timeZone = TimeZone.getDefault()
        val value = ContentValues()
        value.put(CalendarContract.Calendars.NAME, calendarName);

        value.put(CalendarContract.Calendars.ACCOUNT_NAME, CALENDARS_ACCOUNT_NAME)
        value.put(CalendarContract.Calendars.ACCOUNT_TYPE, CALENDARS_ACCOUNT_TYPE)

        val displayName = when(calendarName){
            CALENDARS_NAME_1 -> CALENDARS_DISPLAY_NAME_1
            CALENDARS_NAME_2 -> CALENDARS_DISPLAY_NAME_2
            else -> "No Display Name"
        }
        value.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,displayName)

        val calendarColor = when(calendarName){
            CALENDARS_NAME_1 -> ContextCompat.getColor(context, R.color.colorPrimary)
            CALENDARS_NAME_2 -> ContextCompat.getColor(context, R.color.colorAccent)
            else -> ContextCompat.getColor(context, R.color.colorPrimaryDark)
        }

        value.put(CalendarContract.Calendars.VISIBLE, 1)
        value.put(CalendarContract.Calendars.CALENDAR_COLOR, calendarColor)
        value.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL, CalendarContract.Calendars.CAL_ACCESS_OWNER);
        value.put(CalendarContract.Calendars.SYNC_EVENTS, 1)
        value.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE, timeZone.id)
        value.put(CalendarContract.Calendars.OWNER_ACCOUNT, CALENDARS_ACCOUNT_NAME)
        value.put(CalendarContract.Calendars.CAN_ORGANIZER_RESPOND, 0);

        var calendarUri = Uri.parse(CALANDER_URL);
        calendarUri = calendarUri.buildUpon()
            .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
            .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME,
                CALENDARS_ACCOUNT_NAME
            )
            .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE,
                CALENDARS_ACCOUNT_TYPE
            )
            .build();

        val result = context.contentResolver.insert(calendarUri, value);

        return if (result == null){
            null
        }else{

            val accountId = ContentUris.parseId(result)
            val accountName = CALENDARS_ACCOUNT_NAME
            Log.d(TAG, "id: $accountId, name: $accountName")

            return CalendarAccount(
                accountId = accountId,
                accountName = accountName,
                accountDisplayName = displayName,
                name = calendarName,
                color = calendarColor
            )
        }

    }

    fun deleteCalendarAccount(context: Context, calendarId: Long){


    }
//endregion

    //region calendar events
    fun addCalendarEvent(context: Context, newEvent: CalendarEvent): Long? {
        val event = ContentValues();
        event.put("title", newEvent.title);
        event.put("description", newEvent.description);

        event.put("calendar_id", newEvent.calendarId);

        val mCalendar = Calendar.getInstance();
        mCalendar.timeInMillis = newEvent.startTime
        val begin = mCalendar.time.time
        mCalendar.timeInMillis = newEvent.endTime
        val end = mCalendar.time.time;

        event.put(CalendarContract.Events.DTSTART, begin)
        event.put(CalendarContract.Events.DTEND, end)

        val timeZoneId = TimeZone.getDefault().id.toString()
        event.put(CalendarContract.Events.EVENT_TIMEZONE, timeZoneId);

        if(newEvent.reminderMinute>0 || newEvent.isPushNotificationReminderSet) {
            event.put(CalendarContract.Events.HAS_ALARM, 1)
        }else{
            event.put(CalendarContract.Events.HAS_ALARM, 0)
        }

        return context.contentResolver.insert(Uri.parse(CALANDER_EVENT_URL), event)?.let { newEventUri ->

            val newEventId = ContentUris.parseId(newEventUri)


            var isReminderSet = true

            if(newEvent.reminderMinute > 0) {
                val values = ContentValues()
                values.put(CalendarContract.Reminders.EVENT_ID, ContentUris.parseId(newEventUri))

                values.put(CalendarContract.Reminders.MINUTES, 1);
                values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);

                isReminderSet = context.contentResolver.insert(Uri.parse(CALANDER_REMIDER_URL), values)?.let {
                    true
                } ?: run {
                    false
                }
            }

            var isPushNotificationSet = true
            if(newEvent.isPushNotificationReminderSet){
                val values = ContentValues()

                values.put(CalendarContract.Reminders.EVENT_ID, ContentUris.parseId(newEventUri))

                values.put(CalendarContract.Reminders.MINUTES, MINUTES_OF_24_HOURS);
                values.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALARM);

                isPushNotificationSet = context.contentResolver.insert(Uri.parse(CALANDER_REMIDER_URL), values)?.let {
                    true
                } ?: run {
                    false
                }


            }


            return when(isReminderSet&&isPushNotificationSet){
                true -> newEventId
                false -> null
            }


        } ?: run {
            null
        }
    }

    fun deleteCalendarEvent(context: Context, eventId: Long): Boolean{
        val eventCursor = context.contentResolver.query(Uri.parse(CALANDER_EVENT_URL), null, null, null, null);
        eventCursor.use { _eventCursor ->
            if (_eventCursor == null) {
                return false
            }

            if (_eventCursor.count > 0) {

                val deleteUri = ContentUris.withAppendedId(Uri.parse(CALANDER_EVENT_URL), eventId)

                return (context.contentResolver.delete(deleteUri, null, null) > 0)
            }
        }

        return false
    }

    fun queryCalendarEvents(context: Context, calIdSet: Set<Long> = emptySet()): List<CalendarEvent> {
        val eventCursor = context.contentResolver.query(Uri.parse(CALANDER_EVENT_URL), null, null, null, CalendarContract.Events.DTSTART);
        eventCursor.use { _eventCursor ->
            if (_eventCursor == null) {
                return emptyList()
            }

            val eventList = mutableListOf<CalendarEvent>()
            if (_eventCursor.count > 0) {

                while (_eventCursor.moveToNext()) {
                    val calendarId = _eventCursor.getLong(_eventCursor.getColumnIndex(CalendarContract.Events.CALENDAR_ID))
                    val id = _eventCursor.getLong(_eventCursor.getColumnIndex(CalendarContract.Calendars._ID))
                    val title = _eventCursor.getStringOrNull(_eventCursor.getColumnIndex(CalendarContract.Events.TITLE)) ?: "No Title"
                    val description = _eventCursor.getStringOrNull(_eventCursor.getColumnIndex(CalendarContract.Events.DESCRIPTION)) ?: "No Description"
                    val startTime = _eventCursor.getLong(_eventCursor.getColumnIndex(CalendarContract.Events.DTSTART))
                    val endTime = _eventCursor.getLongOrNull(_eventCursor.getColumnIndex(CalendarContract.Events.DTEND)) ?: -1L
                    val color = _eventCursor.getIntOrNull(_eventCursor.getColumnIndex(CalendarContract.Calendars.CALENDAR_COLOR)) ?: Color.GRAY
                    val calendarDisplayName = _eventCursor.getStringOrNull(_eventCursor.getColumnIndex(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME)) ?: "No Display Name"
                    var reminderMinute = 0
                    var isPushNotificationSet = false

                    val selection = "${CalendarContract.Reminders.EVENT_ID}=$id"
                    val projection = arrayOf(CalendarContract.Reminders.MINUTES, CalendarContract.Reminders.METHOD)
                    val reminderCursor = context.contentResolver.query(Uri.parse(CALANDER_REMIDER_URL), projection, selection, null, null)

                    while(reminderCursor.moveToNext()) {
                        val minute = reminderCursor.getIntOrNull(0) ?: 0
                        val method = reminderCursor.getInt(1) ?: -1

                         when(method){
                            CalendarContract.Reminders.METHOD_ALERT-> {reminderMinute=minute}

                             CalendarContract.Reminders.METHOD_ALARM->{
                                 isPushNotificationSet = (minute== MINUTES_OF_24_HOURS)
                             }
                        }

                    }

                    Log.d(TAG, "$calendarDisplayName -> $reminderMinute, $isPushNotificationSet")

                    if (calIdSet.isEmpty() || calIdSet.contains(calendarId)) {
                        CalendarEvent(
                            calendarId = calendarId,
                            id = id,
                            title = title,
                            description = description,
                            startTime = startTime,
                            endTime = endTime,
                            calendarColor = color,
                            calendarDisplayName = calendarDisplayName,
                            reminderMinute = reminderMinute,
                            isPushNotificationReminderSet = isPushNotificationSet
                        ).let {
                            eventList.add(it)
                        }
                    }
                }


            }

            return eventList
        }



    }
//endregion
}

