package com.example.myeventmanager.utils

import android.util.Log
import com.example.myeventmanager.PUSH_NOTIFICATION_KEY
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.text.SimpleDateFormat
import java.util.*

const val HOOK_TAG = "HookAPI"
const val BASE_URL = "https://api.posthook.io/"
const val HOOK_API_KEY ="0d97a66cf4b143ca95153796c8ed7072"
val HookAPI : IHookAPI by lazy {
    Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build().create(IHookAPI::class.java)
}

interface IHookAPI {
    @POST("v1/hooks")
    fun postHook(@Header("X-API-Key") apiKey:String,  @Body hookBody: HookBody): Observable<Result>

    @DELETE("v1/hooks/{hookId}")
    fun deleteHook(@Header("X-API-Key") apiKey:String,  @Path("hookId") hookId: String): Observable<Result>
}

data class HookBody(
    @SerializedName("path") val path: String,
    @SerializedName("postAt") val postAt: String,
    @SerializedName("data") val data: Map<String, String>
)
data class Result(@SerializedName("data") val data: Data){
    class Data(){
        val id: String = ""
    }

}

private fun generatePathValue(title: String): String?{
//    return "/api/sendPushNotification?title=$title"
    return "/api/sendPushNotification"
}

fun schedulePushNotification(eventTitle: String, startDateTime: Long): Observable<String>{

    val sdf = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
    val utcSdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    utcSdf.timeZone = TimeZone.getTimeZone("UTC")

    Date(startDateTime).let{
        Log.d(HOOK_TAG, "start time = ${sdf.format(it)}, utc = ${utcSdf.format(it)}")
    }


    val _24HR = 24*60*60*1000
    val inAdvanced = _24HR
    val pushDateTime = Date(startDateTime - inAdvanced)

    pushDateTime.let {
        Log.d(HOOK_TAG, "push time = ${sdf.format(it)}, utc = ${utcSdf.format(it)}")
    }

    val timeStr = "${utcSdf.format(pushDateTime)}.000Z"
    Log.d(HOOK_TAG, "UTC Time is: ${timeStr}")

    val title = if (eventTitle.length > 24){
        eventTitle.subSequence(0, 24).toString()
    }else{
        eventTitle
    }

    val hookPath = generatePathValue(title)

    val hookBody = HookBody(
        path = hookPath ?: "",
        postAt = timeStr,
        data = mapOf(
            "title" to title,
            "startTime" to SimpleDateFormat("HH:mm").format(Date(startDateTime)),
            "instanceToken" to PreferencesHelper.instanceToken,
            "key" to PUSH_NOTIFICATION_KEY
        )
    )

    return HookAPI.postHook(HOOK_API_KEY,hookBody).map {result->
        val hookId = result.data.id
        hookId ?: ""
    }

}