package com.example.myeventmanager.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Log
import com.example.myeventmanager.FAV_EVENT_ID_SET_KEY
import com.example.myeventmanager.INSTANCE_ID_KEY
import com.example.myeventmanager.INSTANCE_TOKEN_KEY

object PreferencesHelper {

    private val TAG = "PreferencesHelper"

    private lateinit var sharedPreferences: SharedPreferences

    fun install(context: Context){
//        sharedPreferences = context.getSharedPreferences("MyEventManager", Context.MODE_PRIVATE)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

//        sharedPreferences.registerOnSharedPreferenceChangeListener { sharedPreferences, key ->
//            Log.d(TAG, key)
//        }
    }

    var instanceToken: String = ""
    get(){
        return sharedPreferences.getString(INSTANCE_TOKEN_KEY, "")
    }
    set(newValue){
        field = newValue

        sharedPreferences.edit().putString(INSTANCE_TOKEN_KEY, newValue).apply()
    }

    var instanceId: String = "instanceId"
        get(){
            return sharedPreferences.getString(INSTANCE_ID_KEY, "")
        }
        set(newValue){
            field = newValue

            sharedPreferences.edit().putString(INSTANCE_ID_KEY, newValue).apply()
        }

//    fun addFavEventId(eventId: Long): Boolean{
//
//        return sharedPreferences.getStringSet(FAV_EVENT_ID_SET_KEY, mutableSetOf()).takeIf { it.size < 3 }?.let {
//            it.add(eventId.toString())
//            sharedPreferences.edit().putStringSet(FAV_EVENT_ID_SET_KEY, it).commit()
//        } ?: kotlin.run {
//            false
//        }
//
//    }
//
//    fun removeFavEventId(eventId: Long): Boolean{
//        val favIdSet = sharedPreferences.getStringSet(FAV_EVENT_ID_SET_KEY, mutableSetOf())
//        favIdSet.remove(eventId.toString())
//        return sharedPreferences.edit().putStringSet(FAV_EVENT_ID_SET_KEY, favIdSet).commit()
//    }
//
//    fun getFavEventIds(): Set<Long>{
//
//        val eventIdSet = mutableSetOf<Long>()
//        sharedPreferences.getStringSet(FAV_EVENT_ID_SET_KEY, emptySet()).forEach {
//                eventIdSet.add(it.toLong())
//        }
//
//        return eventIdSet
//    }
//
//    fun saveHookId(eventId: Long, hookId: String): Boolean{
//        return sharedPreferences.edit().putString("EVENT_ID_$eventId", hookId).commit()
//    }
//
//    fun loadHookId(eventId: Long): String?{
//        return sharedPreferences.getString("EVENT_ID_$eventId", null)
//    }
//
//    fun deleteHookId(eventId: Long){
//        sharedPreferences.edit().remove("EVENT_ID_$eventId").apply()
//    }
}